local VictoryDefeat = require 'view.victory_defeat'
local State = require 'state'
local PALETTE_DB = require 'database.palette'

local GameEndState = require 'common.class' (State)

function GameEndState:_init(stack)
  self:super(stack)
  self.message = nil
end

function GameEndState:enter(params)
  love.graphics.setBackgroundColor(PALETTE_DB.black)
  self.message = VictoryDefeat(params.message)
  self:view('hud'):add("victory_or_defeat", self.message)
end

function GameEndState:leave()
  self:view('hud'):remove("victory_or_defeat")
end

function GameEndState:on_keypressed(key)
  if key == 'return' then
    return self:pop()
  end
end

return GameEndState