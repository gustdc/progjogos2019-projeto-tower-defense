
local Wave = require 'model.wave'
local Unit = require 'model.unit'
local Vec = require 'common.vec'
local Cursor = require 'view.cursor'
local Alert = require 'view.alert'
local UnitStats = require 'view.unit_stats'
local SpriteAtlas = require 'view.sprite_atlas'
local ActionMenu = require 'view.action_menu'
local BattleField = require 'view.battlefield'
local TileDebugger = require 'view.tile_debugger'
local Stats = require 'view.stats'
local State = require 'state'

local PlayStageState = require 'common.class' (State)

PlayStageState.gameTick = 1

function PlayStageState:_init(stack)
  self:super(stack)
  self.game_time = nil
  self.stage = nil
  self.cursor = nil
  self.atlas = nil
  self.battlefield = nil
  self.units = nil
  self.wave = nil
  self.current_wave = nil
  self.wave_cooldown = nil
  self.stats = nil
  self.monsters = nil
  self.gold = nil
  self.tile_debugger = nil
  self.timer = 0
  self.unit_specs = nil
end

function PlayStageState:enter(params)
  self.stage = params.stage
  self.game_time = 0
  self.current_wave = 0
  self.wave_cooldown = 11
  self.gold = self.stage.starting_gold
  self:_load_view()
  self:_load_units()
end

function PlayStageState:leave()
  self:view('bg'):remove('battlefield')
  self:view('fg'):remove('atlas')
  self:view('bg'):remove('cursor')
  self:view('hud'):remove('stats')
  self:view('hud'):remove('menu')
  self:view('hud'):remove('alerts')
  self:view('bg'):remove('character_stats')
end

function PlayStageState:_load_view()
  self.battlefield = BattleField()
  self.atlas = SpriteAtlas()
  self.cursor = Cursor(self.battlefield)
  self.alert = Alert(1)
  self.tile_debugger = TileDebugger(self.battlefield)
  self.action_menu = ActionMenu(self.battlefield.bounds, self.atlas, self.stage.units,
    self.stage.upgrades)
  local _, right, top, _ = self.battlefield.bounds:get()
  self.stats = Stats(Vec(right + 16, top), self.gold)
  self:view('bg'):add('battlefield', self.battlefield)
  self:view('fg'):add('atlas', self.atlas)
  self:view('bg'):add('cursor', self.cursor)
  self:view('hud'):add('stats', self.stats)
  self:view('hud'):add('menu', self.action_menu)
  self:view('hud'):add('alerts', self.alert)
  --self:view('hud'):add('tile_debugger', self.tile_debugger)
end

function PlayStageState:_load_units()
  local castle_pos = self.stage.castle_location
  local pos = self.battlefield:tile_to_screen(castle_pos[1], castle_pos[2])
  self.units = {}
  self.unit_specs = {}
  for _, unit_name in ipairs(self.stage.units) do
    self.unit_specs[unit_name] = require('database.units.' .. unit_name)
  end
  local capital = self:_create_unit_at('capital', pos)
  table.insert(self.units, capital)
  self:_next_wave()
  self.monsters = {}
end

function PlayStageState:_create_unit_at(specname, pos)
  local spec = self.unit_specs[specname]
  if spec == nil then
    spec = require('database.units.' .. specname)
  end
  local unit = Unit(specname, spec, pos, self.battlefield)
  self.atlas:add(unit, pos, unit:get_appearance())
  return unit
end

function PlayStageState:_next_wave()
  self.current_wave = self.current_wave + 1
  if not (self.current_wave > #self.stage.waves) then
    self.wave = Wave(self.stage.waves[self.current_wave])
    print("Start wave " .. self.current_wave)
    self.wave:start()
  end
end

function PlayStageState:on_mousepressed(_, _, button)
  local curr_opt = self.action_menu:current_option()
  if button == 1 then
    if curr_opt  == 0 then
      self:look_at_cell(Vec(self.cursor:get_position()))
    elseif curr_opt == 1 then
      self:sell_unit_at_cell(Vec(self.cursor:get_position()))
    elseif curr_opt <= #self.stage.units + 1 then
      -- Buy a unit
      local unit_name = self.stage.units[curr_opt - 1]
      self:buy_unit(unit_name)
      self.action_menu:reset_menu()
    else
      local upgrade_name = self.stage.upgrades[curr_opt - #self.stage.units - 1]
      self:buy_upgrade(upgrade_name)
      self.action_menu:reset_menu()
    end
  elseif button == 2 then
    if curr_opt ~= 0 then
      self.action_menu:reset_menu()
    else
      self:look_at_cell(nil)
    end
  end
end

function PlayStageState:sell_unit_at_cell(cell)
  for i, unit in ipairs(self.units) do
      if (unit.pos.x == cell.x and unit.pos.y == cell.y and i ~= 1) then
        self.atlas:remove(unit)
        self.gold = self.gold + math.floor(2 * unit.cost / 3)
        self.stats:update_gold(self.gold)
        table.remove(self.units, i)
        return
      end
    end
end

function PlayStageState:look_at_cell(cell)
  self.cursor:highlight_cell(cell)
  if cell ~= nil then
    for _, unit in ipairs(self.units) do
      if (unit.pos.x == cell.x and unit.pos.y == cell.y) then
        local stats = UnitStats(self.battlefield.bounds, unit, self.battlefield)
        self:view('bg'):add('character_stats', stats)
        return
      end
    end
  end
  self:view('bg'):remove('character_stats')
end

function PlayStageState:buy_unit(unit_name)
  local unit_spec = self.unit_specs[unit_name]
  local unit_cost = unit_spec.cost
  local curr_pos  = Vec(self.cursor:get_position())
  if self:_tile_is_free(curr_pos) then
    if self.gold >= unit_cost then
      self.gold = self.gold - unit_cost
      local unit = self:_create_unit_at(unit_name, curr_pos)
      self.tile_debugger:add(unit)
      table.insert(self.units, unit)
      self.stats:update_gold(self.gold)
    else
      self.alert:add_message(string.format("Not enough gold for %s!", unit_name))
    end
  else
    self.alert:add_message("Cell is already in use!")
  end
end

function PlayStageState:buy_upgrade(upgrade_name)
  local upgrade = require('database.upgrades.' .. upgrade_name)
  if self.gold >= upgrade.cost then
    self.gold = self.gold - upgrade.cost
    self.stats:update_gold(self.gold)
    self:apply_upgrade(upgrade)
    self.alert:add_message(string.format("Bought upgrade for %s!", upgrade.name))
  else
    self.alert:add_message(string.format("Not enough gold for %s!", upgrade.name))
  end
end

function PlayStageState:apply_upgrade(upgrade)
  if upgrade.effect == 'raise_hp' then
    for _, unit in ipairs(self.units) do
      if unit.specname == upgrade.target then
        unit.hp = unit.hp + upgrade.power
        unit.max_hp = unit.max_hp + upgrade.power
      end
    end
    self.unit_specs[upgrade.target].max_hp = self.unit_specs[upgrade.target].max_hp + upgrade.power
  elseif upgrade.effect == 'raise_range' then
    for _, unit in ipairs(self.units) do
      if unit.specname == upgrade.target then
        unit:set_range(unit.spec.range + upgrade.power)
      end
    end
    self.unit_specs[upgrade.target].range =
      self.unit_specs[upgrade.target].range + upgrade.power
  elseif upgrade.effect == 'raise_effect_power' then
    for _, unit in ipairs(self.units) do
      if unit.specname == upgrade.target then
        unit.effect.effect_power = unit.effect.effect_power + upgrade.power
      end
    end
    self.unit_specs[upgrade.target].effect.effect_power =
      self.unit_specs[upgrade.target].effect.effect_power + upgrade.power
  end
end

function PlayStageState:_tile_is_free(cell)
  for _, unit in ipairs(self.units) do
    if (unit.pos.x == cell.x and unit.pos.y == cell.y) then
      return false
    end
  end
  return true
end

function PlayStageState:on_keypressed(key)
  for number = 1,9 do
    if key == number .. "" then
      self.action_menu:select_option(number)
      break
    end
  end
end

function PlayStageState:update(dt)
  self:count_time(dt)
  self:move_monsters(dt)
  for _, unit in ipairs(self.units) do
    unit:detect_enemies(self.monsters, dt)
  end
  self.monsters = self:remove_dead_enemies(self.monsters)
  for _, monster in ipairs(self.monsters) do
    monster:detect_enemies(self.units, dt)
  end
  self.units = self:remove_dead_enemies(self.units)
end

function PlayStageState:move_monsters(dt)
  local sprite_instance, objective_pos, hipotenuse
  local castle_pos = self.units[1].pos
  for _, monster in ipairs(self.monsters) do
    if not monster:detected_castle() then
      sprite_instance = self.atlas:get(monster)
      objective_pos = castle_pos - sprite_instance.position
      hipotenuse = math.sqrt(objective_pos.x * objective_pos.x
                             + objective_pos.y * objective_pos.y)
      objective_pos = objective_pos / hipotenuse
      objective_pos.x = math.modf(objective_pos.x * 10)/10
      objective_pos.y = math.modf(objective_pos.y * 10)/10
      sprite_instance.position:add(objective_pos * monster.speed * dt)
    end
  end
end

function PlayStageState:count_time(dt)
  if self.timer < 0 then
    self.game_time = self.game_time + 1
    self.stats:update_time(self.game_time)
    self.timer = PlayStageState.gameTick

    self.wave:increment_wave_cooldown(true)

    local pending, monster, next_wave = self.wave:poll()

    if next_wave and #self.monsters == 0 then
      if self.current_wave == #self.stage.waves then
        self:pop({ message = "Você derrotou todas as tropas inimigas! Parabéns!" })
      end
      self.wave_cooldown = self.wave_cooldown - 1
      self.stats:update_wave_cooldown(self.wave_cooldown)
      if self.wave_cooldown == 0 then
        self.wave_cooldown  = 11
        self:_next_wave()
      end
    end

    local rand = love.math.random
    while pending > 0 do
      local x, y = rand(5, 7), -rand(5, 7)
      local pos = self.battlefield:tile_to_screen(x, y)
      monster = self:_create_unit_at(monster, pos)
      self.tile_debugger:add(monster)
      table.insert(self.monsters, monster)
      pending = pending - 1
    end

    self:units_action()

  else
    self.timer = self.timer - dt
  end
end

function PlayStageState:units_action()
  for _, unit in ipairs(self.units) do
    if unit:get_effect_type() == 'gold_building' then
      if unit:increment_effect_cooldown() then
        self.gold = self.gold + unit:get_effect_power()
        self.stats:update_gold(self.gold)
      end
    end
    if unit:get_effect_type() == 'heal' then
      if unit:increment_effect_cooldown() then
        local friends_in_range = unit:detect_friends(self.units)
        for _, friend in ipairs(friends_in_range) do
          local _, max_hp = friend:get_hp()
          if friend.hp < max_hp then
            friend.hp = math.min(friend.hp + unit:get_effect_power(), max_hp)
          end
        end
      end
    end
  end
end

function PlayStageState:remove_dead_enemies(enemies)
  local enemies_alive = {}
  local count = 1
  for _, enemy in ipairs(enemies) do
    if enemy.hp > 0 then
      enemies_alive[count] = enemy
      count = count + 1;
    else
      if enemy.name == "Capital" then
        self:pop({ message = "Seu castelo foi invadido e você foi derrotado!" })
      end
      self.atlas:remove(enemy)
    end
  end

  return enemies_alive
end


return PlayStageState

