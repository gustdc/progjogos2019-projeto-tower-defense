
local TileDebugger = require 'common.class' ()

local CELL_SIZE = 36

function TileDebugger:_init(battlefield)
  self.position = nil
  self.units = {}
  self.battlefield = battlefield
end

function TileDebugger:add(unit)
  table.insert(self.units, unit)
end

function TileDebugger:draw()
  local g = love.graphics
  for _, unit in ipairs(self.units) do
      g.push()
      local position = self.battlefield:round_to_tile(unit:get_position())
      local offset = position
      g.translate(offset:get())
      g.setColor(1, 1, 1, .4)
      g.rectangle('fill', -CELL_SIZE / 2, -CELL_SIZE / 2, CELL_SIZE, CELL_SIZE)
      g.pop()
--    for i = 1, #distance_check do
--      g.push()
--      local position = self.battlefield:round_to_tile(unit:get_position())
--      local offset = position + distance_check[i] * CELL_SIZE
--      g.translate(offset:get())
--      g.setColor(1, 1, 1, .4)
--      g.rectangle('fill', -CELL_SIZE / 2, -CELL_SIZE / 2, CELL_SIZE, CELL_SIZE)
--      g.pop()
--    end
  end
end

return TileDebugger

