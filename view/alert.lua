
local Alert = require 'common.class' ()
local Vec = require 'common.vec'

function Alert:_init(time)
  self.time = time
  self.timer = 0
  self.messages = {}
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 18)
  self.font:setFilter('nearest', 'nearest')
  self.position = Vec(60, 60)
end

function Alert:add_message(message)
  table.insert(self.messages, (message))
  self.timer = self.timer + self.time
end

function Alert:remove_message(index)
  self.messages[index] = nil
end

function Alert:draw()
  local g = love.graphics
  g.push()
  g.setFont(self.font)
  g.setColor(1, 1, 1)
  g.translate(self.position:get())
  for _, message in ipairs(self.messages) do
    g.print(message)
    g.translate(0, self.font:getHeight())
  end

  g.pop()
end

function Alert:update(dt)
  if #self.messages > 0  then
    if self.timer < 0 then
      self.messages = {}
      self.timer = self.time
    else
      self.timer = self.timer - dt
    end
  end
end

return Alert
