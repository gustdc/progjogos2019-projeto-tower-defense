
local Vec = require 'common.vec'
local PALETTE_DB = require 'database.palette'
local UnitStats = require 'common.class' ()

UnitStats.BUTTON_GAP = 12
UnitStats.PADDING = Vec(8, 8)
UnitStats.BUTTON_SIZE = Vec(40, 40)

function UnitStats:_init(screen_bounds, unit, battlefield)
  local _, right, top, _ = screen_bounds:get()
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 18)
  self.font:setFilter('nearest', 'nearest')
  self.position = Vec(right + 8 , top + 160)
  self.size = Vec(220, 160)
  self.unit = unit
  self.battlefield = battlefield
end

function UnitStats:draw()
  local g = love.graphics
  g.push()
  g.translate(self.position:get())
  g.setFont(self.font)
  g.setColor(1, 1, 1)
  g.setLineWidth(4)
  g.rectangle('line', 0, 0, self.size:get())
  g.translate(UnitStats.PADDING:get())
  if (self.unit.cost > 0) then
    g.print(string.format("%s (%dG)",self.unit.name, self.unit.cost))
    g.translate(0, self.font:getHeight())
  else
    g.print(string.format("%s",self.unit.name))
    g.translate(0, self.font:getHeight())
  end
  g.print(string.format("HP: %d/%d", self.unit.hp, self.unit.max_hp))
  g.translate(0, self.font:getHeight())
  if (self.unit.damage > 0) then
    g.print(string.format("Damage: %d",self.unit.damage))
    g.translate(0, self.font:getHeight())
  end

  if (self.unit.effect) then
    local message = string.format(self.unit.effect.description,
      self.unit.effect.effect_power, self.unit.effect.cooldown)
    local effect_message = love.graphics.newText(self.font, message)
    --g.draw(effect_message, 0, 0)
    g.printf(message, 0, 0, 200)
    g.translate(0, effect_message:getDimensions())
  end
  self:draw_unit_ranges(g)
  g.pop()
end

function UnitStats:draw_unit_ranges(g)
  local unit_cell = self.battlefield:round_to_tile(self.unit.pos)
  g.origin()
  g.setLineWidth(1)
  g.setColor(PALETTE_DB['red'])
  love.graphics.circle('line', unit_cell.x, unit_cell.y, self.unit.detect_range + 32)
  if self.unit:has_effect() then
    g.setColor(PALETTE_DB['green'])
    love.graphics.circle('line', unit_cell.x, unit_cell.y, self.unit:get_effect_range())
  end
end

return UnitStats
