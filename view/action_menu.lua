local Vec = require 'common.vec'

local ActionMenu = require 'common.class' ()

ActionMenu.BUTTON_GAP = 12
ActionMenu.PADDING = Vec(8, 8)
ActionMenu.BUTTON_SIZE = Vec(40, 40)

function ActionMenu:_init(screen_bounds, atlas, units, upgrades)
  --local center =
  local _, right, _, bottom = screen_bounds:get()
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 20)
  self.font:setFilter('nearest', 'nearest')
  self.position = Vec(right + 8 , bottom - 180)
  self.options = {}
  self.size = Vec(220, 180)
  self.selected = 0
  self.atlas = atlas
  self:setup_action_menu(units, upgrades)
end

function ActionMenu:setup_action_menu(units, upgrades)
  local
  function new_button(sprite, text)
    return {
      sprite = sprite,
      text = text,
      last = false,
      now = false
    }
  end

  table.insert(self.options, new_button("sell_button", "Sell a unit"))
  for _, unit in ipairs(units) do
    local unit_spec = require('database.units.' .. unit)
    table.insert(self.options, new_button(unit .. "_button",
                 unit_spec.button_description .. string.format("(%dg)", unit_spec.cost)))
  end

  for _, upgrade in ipairs(upgrades) do
    local upgrade_spec = require('database.upgrades.' .. upgrade)
    table.insert(self.options, new_button(upgrade .. "_button",
                 upgrade_spec.button_description .. string.format("(%dg)", upgrade_spec.cost)))
  end

end

function ActionMenu:reset_menu()
  self.selected = 0
end

function ActionMenu:current_option()
  return self.selected
end

function ActionMenu:select_option(index)
  if index <= #self.options then
    self.selected = index
  end
end

function ActionMenu:draw()
  local g = love.graphics
  local bcolor = {0.3, 0.3, 0.3}
  local bcolor_hot = {0.9, 0.9, 0.9}
  local mx, my = love.mouse.getPosition()
  g.push()
  g.translate(self.position:get())
  g.setColor(1, 1, 1)
  g.setLineWidth(4)
  g.rectangle('line', 0, 0, self.size:get())
  for i, option in ipairs(self.options) do
    option.last = option.now
    g.push()
    g.origin()
    local bx, by = self.position:get()
    local row = math.floor(i / 5) + 1
    local col = math.floor(i % 4)
    if col == 0 then
      col = 4
    end

    bx = bx + (ActionMenu.BUTTON_GAP * col - 1) + (ActionMenu.BUTTON_SIZE.x * (col - 1))
    by = by + ActionMenu.BUTTON_GAP +
              ((ActionMenu.BUTTON_GAP + ActionMenu.BUTTON_SIZE.y) * (row - 1))

    local spritex = bx + ActionMenu.PADDING.x * 2 + 4
    local spritey = by + ActionMenu.PADDING.y * 2 + 4

    local hot = mx > bx and mx < bx + 44 and
                my > by and my < by + 44

    local bsprite
    if hot or self.selected == i then
      g.setColor(unpack(bcolor_hot))
      bsprite = option.sprite .. "_highlight"
      g.print(option.text, self.font, self.position.x, self.position.y + self.size.y)
    else
      g.setColor(unpack(bcolor))
      bsprite = option.sprite
    end

    option.now = love.mouse.isDown(1)

    if hot and not option.last and option.now then
      self.selected = i
    end

    -- Draw button sprite and rectangle border
    g.rectangle('line', bx, by, ActionMenu.BUTTON_SIZE:get())
    self.atlas:add(option.sprite .. "_opt", Vec(spritex , spritey), bsprite)
    g.pop()
  end
  g.pop()
end

return ActionMenu
