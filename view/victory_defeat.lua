
local VictoryDefeat = require 'common.class' ()

function VictoryDefeat:_init(message)
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 36)
  self.font:setFilter('nearest', 'nearest')
  self.message = love.graphics.newText(self.font, message)
end

function VictoryDefeat:draw()
  local g = love.graphics
  local width, _ = g.getDimensions()
  local text_w, _ = self.message:getDimensions()
  g.push()
  g.setColor(1, 1, 1)
  g.setLineWidth(4)
  g.translate(width/2 - text_w/2, 200)
  g.draw(self.message, 0, 0)
  g.translate(0, self.font:getHeight())
  g.translate(0, self.font:getHeight())
  g.print("Pressione enter para voltar ao menu de aventuras.")
  g.pop()
end

return VictoryDefeat

