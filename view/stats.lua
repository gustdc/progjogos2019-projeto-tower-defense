
local Stats = require 'common.class' ()

function Stats:_init(position, gold)
  self.position = position
  self.time = 0
  self.gold = gold
  self.cooldown = 0
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 20)
  self.font:setFilter('nearest', 'nearest')
end

function Stats:update_gold(gold)
  self.gold = gold
end

function Stats:update_time(time)
  self.time = time
end

function Stats:update_wave_cooldown(cooldown)
  self.cooldown = cooldown
end

function Stats:draw()
  local g = love.graphics
  g.push()
  g.setFont(self.font)
  g.setColor(1, 1, 1)
  g.translate(self.position:get())
  g.print(("Gold %d"):format(self.gold))
  g.translate(0, self.font:getHeight())
  g.print(("Time: %s"):format(self.time))
  g.translate(0, 2 * self.font:getHeight())
  g.print("Defeat all enemies")
  g.translate(0, self.font:getHeight())
  g.print("enemies to advance!")
  if self.cooldown > 0 then
    g.translate(0, 2 * self.font:getHeight())
    g.print(("Prepare for next attack: %d"):format(self.cooldown))
  end
  g.pop()
end

return Stats
