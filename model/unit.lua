
local Vec = require 'common.vec'
local Unit = require 'common.class' ()
local copy_table = require 'common.copy_table'
Unit.distance_check = {
  Vec(-1, -1),
  Vec(-1,  0),
  Vec(-1,  1),
  Vec( 0, -1),
  Vec( 0,  1),
  Vec( 1, -1),
  Vec( 1,  0),
  Vec( 1,  1)
}

Unit.CELL_SIZE = 32

function Unit:_init(specname, spec, pos, battlefield)
  self.spec = spec
  self.specname = specname
  self.name = self.spec.name
  self.cost = self.spec.cost
  self.max_hp = self.spec.max_hp
  self.hp = self.spec.max_hp
  self.damage = self.spec.damage
  self.effect = copy_table(self.spec.effect)
  self.attack_cooldown = 2
  self.effect_cooldown = 0
  self.appearance = self.spec.appearance
  self.button_description = self.spec.button_description
  self.pos = pos
  self.detect_range = Unit.CELL_SIZE * self.spec.range
  if self.spec.speed then
    self.speed = self.spec.speed
  end
  self.battlefield = battlefield
  self.detected_capital = false
end

function Unit:get_name()
  return self.name
end

function Unit:get_hp()
  return self.hp, self.max_hp
end

function Unit:get_appearance()
  return self.appearance
end

function Unit:get_position()
  return self.pos
end

function Unit:get_effect_type()
  if self.effect then
    return self.effect.type
  else
    return nil
  end
end

function Unit:has_effect()
    return self.effect ~= nil
end

function Unit:get_effect_power()
  if self.effect then
    return self.effect.effect_power
  else
    return nil
  end
end

function Unit:get_effect_range()
  if self.effect then
    return (self.effect.effect_range + 1) * Unit.CELL_SIZE
  else
    return nil
  end
end

function Unit:set_range(new_range)
  self.detect_range = new_range * Unit.CELL_SIZE
end

function Unit:detect_enemies(enemies, dt)
  for _, enemy in ipairs(enemies) do
    -- Monstros tem valor booleano
    local enemy_pos = self.battlefield:round_to_tile(enemy.pos)
    local self_pos = self.battlefield:round_to_tile(self.pos)
    local distance = self_pos - enemy_pos
    if math.abs(distance.x) <= self.detect_range and math.abs(distance.y) <= self.detect_range then
      --print(self.name .. ' detectou ' .. enemy.name)
      if enemy.name == "Capital" then
        self.detected_capital = true
      end
      enemy:take_damage(self.damage * dt)
    end
  end
end

function Unit:detect_friends(friends)
  local friends_in_range = {}
  for _, unit in ipairs(friends) do
    local unit_pos = self.battlefield:round_to_tile(unit.pos)
    local self_pos = self.battlefield:round_to_tile(self.pos)
    local distance = self_pos - unit_pos
    local range = self.effect.effect_range * Unit.CELL_SIZE
    if math.abs(distance.x) <= range and math.abs(distance.y) <= range then
      if unit.name ~= "Capital" then
        table.insert(friends_in_range, unit)
      end
    end
  end

  return friends_in_range
end

function Unit:detected_castle()
  return self.detected_capital
end

function Unit:take_damage(damage)
  if damage > self.hp then
    self.hp = 0
  else
    self.hp = self.hp - damage
  end
end

-- Returns true if unit can use effect
function Unit:increment_effect_cooldown()
  self.effect_cooldown = self.effect_cooldown + 1
  if self.effect_cooldown == self.effect.cooldown then
    self.effect_cooldown = 0
    return true
  else
    return false
  end
end

return Unit

