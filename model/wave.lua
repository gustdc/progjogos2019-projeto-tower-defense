
local Wave = require 'common.class' ()

function Wave:_init(spawns)
  self.spawns = spawns
  self.delay = 3
  self.left = 0
  self.pending = 0
  self.spawn_halt = false
  self.current_spawn = 1
  self.current_spawn_monster = nil
  self.current_spawn_qnty = nil
end

function Wave:start()
  self.current_spawn_monster = self.spawns[self.current_spawn][1]
  self.current_spawn_qnty = self.spawns[self.current_spawn][2]
end

function Wave:increment_wave_cooldown()
  self.left = self.left + 1
  if self.left == self.delay then
    self.left = 0
    self.pending = self.pending + 1
    if not self.spawn_halt then self:_spawn_monster() end
  end
end

function Wave:_spawn_monster()
  if self.current_spawn_qnty == 0 then
    self.current_spawn = self.current_spawn + 1
    if self.current_spawn > #self.spawns then
      self.spawn_halt = true
      return
    end
    self.current_spawn_monster = self.spawns[self.current_spawn][1]
    self.current_spawn_qnty = self.spawns[self.current_spawn][2]
  end

  --Para debug do spawn de monstros
  --print('------------------------------')
  --print('Halt      : ' .. tostring(self.spawn_halt))
  --print('Curr spawn: ' .. self.current_spawn)
  --print('Curr mster: ' .. self.current_spawn_monster)
  --print('Curr qnty : ' .. self.current_spawn_qnty)
  --print('------------------------------')

  self.current_spawn_qnty = self.current_spawn_qnty - 1
end

function Wave:poll()
  local pending = self.pending
  self.pending = 0

  if self.spawn_halt then
    pending = 0
  end

  return pending, self.current_spawn_monster, self.spawn_halt
end

return Wave

