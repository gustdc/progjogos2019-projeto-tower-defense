-- Returns copy of a table
local function copy_table(tab)
	local new_table = {}
	if (tab) then
		for i, val in pairs(tab) do
			new_table[i] = val
		end
		return new_table
	else
		return nil
	end
end

return copy_table