return {
	name = 'More Crops',
	target = 'plantation',
	effect = 'raise_effect_power',
	power  = 5,
	cost = 25,
	button_description = "Upgrade Plantations gain"
}
