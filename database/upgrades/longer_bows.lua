return {
	name = "Longer Bows",
	target = 'archer',
	effect = 'raise_range',
	power  = 2,
	cost = 20,
	button_description = "Upgrade Archers range"
}