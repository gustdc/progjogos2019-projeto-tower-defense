return {
	name = 'Shinnier Armor',
	target = 'warrior',
	effect = 'raise_hp',
	power  = 50,
	cost = 15,
	button_description = "Upgrade Warriors' HP"
}