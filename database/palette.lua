
return {
  black = { .1, .1, .1 },
  red = { 1, .4, .4 },
  yellow = {1, 1, 0 },
  green = { .4, 1, .4 },
  blue = { .4, .4, 1 },
  white = { .7, .7, .9 },
  gray = { .3, .3, .3 },
  bright_white = { 1, 1, 1},
  brown = {.5,.5, .1}
}

