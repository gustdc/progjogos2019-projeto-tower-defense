
return {
  name = "Green Slime",
  max_hp = 50,
  damage = 4,
  range = 1,
  speed = 15,
  appearance = 'green_slime'
}

