
return {
  name = "Red Slime",
  max_hp = 15,
  damage = 10,
  range = 1,
  speed = 25,
  appearance = 'red_slime'
}

