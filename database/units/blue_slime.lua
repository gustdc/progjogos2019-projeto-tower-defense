
return {
  name = "Blue Slime",
  max_hp = 70,
  damage = 5,
  range = 1,
  speed = 20,
  appearance = 'blue_slime'
}

