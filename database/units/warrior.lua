
return {
  name = "Warrior Troop",
  cost = 5,
  max_hp = 100,
  damage = 7,
  range = 1,
  appearance = 'knight',
  button_description = 'Deploy a warrior'
}

