
return {
  name = "Priest Troop",
  cost = 15,
  max_hp = 25,
  damage = 2,
  range = 0,
  appearance = 'priest',
  effect = {
    type = 'heal',
    cooldown = 3,
    effect_power = 5,
    effect_range = 2,
    description = "Heals nearby allies for %d hp every %d seconds"
  },
  button_description = 'Deploy a priest'
}

