
return {
  name = "Archer Troop",
  cost = 15,
  max_hp = 8,
  damage = 1,
  range = 2,
  appearance = 'archer',
  button_description = 'Deploy an archer'
}

