
return {
  name = "Plantation",
  cost = 50,
  max_hp = 30,
  damage = 0,
  range = 0,
  appearance = 'plantation',
  effect = {
    type = 'gold_building',
    cooldown = 3,
    effect_power = 5,
    effect_range = 0,
    description = "Generates %d gold every %d seconds"
  },
  button_description = 'Build a plantation'
}

