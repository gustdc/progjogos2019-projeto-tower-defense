
return {
  title = 'Slime Infestation',
  starting_gold = 50,
  castle_location = { -6, -6 },
  units = {'warrior', 'priest', 'archer', 'plantation'},
  upgrades = {'shinnier_armor', 'longer_bows', 'more_crops'},
  waves = {
    {
      { 'blue_slime',  1 },
      { 'green_slime', 2 }
    },
    {
      { 'green_slime', 2 },
      { 'red_slime', 4 }
    },
    {
      { 'blue_slime', 5 }
    },
  }
}

