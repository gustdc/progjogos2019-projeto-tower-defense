
return {
  title = 'Village Last Stand',
  starting_gold = 60,
  castle_location = { -6, 6 },
  units = {'warrior', 'archer', 'plantation'},
  upgrades = {'shinnier_armor', 'longer_bows', 'more_crops'},
  waves = {
    {
      { 'green_slime', 2 },
      { 'blue_slime',  4 },
      { 'green_slime', 2 },
      { 'blue_slime',  1 },
      { 'red_slime', 3 }
    },
    {
      { 'green_slime', 2 },
      { 'red_slime', 4 }
    },
    {
      { 'blue_slime', 5 }
    },
  }
}
