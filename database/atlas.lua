
return {
  texture = 'kenney-1bit.png',
  frame_width = 16,
  frame_height = 16,
  gap_width = 1,
  gap_height = 1,
  sprites = {
    invalid = {
      frame = { 24, 25 },
      color = 'red'
    },
    citadel = {
      frame = { 5, 19 },
      color = 'white'
    },
    cursor = {
      frame = { 29, 14 },
      color = 'gray'
    },
    green_slime = {
      frame = { 27, 8 },
      color = 'green'
    },
    blue_slime = {
      frame = { 27, 8 },
      color = 'blue'
    },
    red_slime = {
      frame = { 27, 8 },
      color = 'red'
    },
    knight = {
      frame = { 28, 0 },
      color = 'blue'
    },
    archer = {
      frame = { 32, 0 },
      color = 'red'
    },
    priest = {
      frame = { 24, 0 },
      color = 'white'
    },
    plantation = {
      frame = { 18, 10 },
      color = 'green'
    },
    upgrade_button = {
      frame = { 28, 21 },
      color = 'gray'
    },
    upgrade_button_highlight = {
      frame = { 28, 21 },
      color = 'bright_white'
    },
    warrior_button = {
      frame = { 28, 0 },
      color = 'gray'
    },
    warrior_button_highlight = {
      frame = { 28, 0 },
      color = 'blue'
    },
    priest_button = {
      frame = { 24, 0 },
      color = 'gray'
    },
    priest_button_highlight = {
      frame = { 24, 0 },
      color = 'white'
    },
    archer_button = {
      frame = { 32, 0 },
      color = 'gray'
    },
    archer_button_highlight = {
      frame = { 32, 0 },
      color = 'red'
    },
    sell_button = {
      frame = { 19, 28 },
      color = 'gray'
    },
    sell_button_highlight = {
      frame = { 19, 28 },
      color = 'yellow'
    },
    plantation_button = {
      frame = { 18, 10 },
      color = 'gray'
    },
    plantation_button_highlight = {
      frame = { 18, 10 },
      color = 'green'
    },
    shinnier_armor_button = {
      frame = { 4, 23 },
      color = 'gray'
    },
    shinnier_armor_button_highlight = {
      frame = { 4, 23 },
      color = 'blue'
    },
    longer_bows_button = {
      frame = { 9, 28 },
      color = 'gray'
    },
    longer_bows_button_highlight = {
      frame = { 9, 28 },
      color = 'brown'
    },
    more_crops_button = {
      frame = { 16, 27 },
      color = 'gray'
    },
    more_crops_button_highlight = {
      frame = { 16, 27 },
      color = 'green'
    }
  }
}

